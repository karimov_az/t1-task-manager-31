package ru.t1.karimov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.command.data.AbstractDataCommand;
import ru.t1.karimov.tm.command.data.DataBackupLoadCommand;
import ru.t1.karimov.tm.command.data.DataBackupSaveCommand;
import ru.t1.karimov.tm.exception.AbstractException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void load() throws AbstractException {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    @SneakyThrows
    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void start() throws AbstractException  {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop () {
        es.shutdown();
    }

}
