package ru.t1.karimov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;

public interface ICommand {

    void execute() throws AbstractException;

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

}
