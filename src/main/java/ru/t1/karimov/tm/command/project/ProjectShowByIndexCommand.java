package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[PROJECT LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

}
