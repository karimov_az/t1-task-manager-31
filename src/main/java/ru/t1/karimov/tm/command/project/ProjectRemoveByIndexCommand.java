package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}
